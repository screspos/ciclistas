<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Puerto $model */
/** @var yii\widgets\ActiveForm $form */
?>
<p><?= Html::a('Volver', ['etapa/index'], ['class' => 'btn btn-outline-success']) ?></p>
<div class="puerto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nompuerto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'altura')->textInput() ?>

    <?= $form->field($model, 'categoria')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pendiente')->textInput() ?>

    <?= $form->field($model, 'numetapa')->textInput() ?>

    <?= $form->field($model, 'dorsal')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-outline-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
