<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Puerto $model */

$this->title = $model->nompuerto;
$this->params['breadcrumbs'][] = ['label' => 'Puertos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="puerto-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'nompuerto' => $model->nompuerto], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'nompuerto' => $model->nompuerto], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <p><?= Html::a('Volver', ['etapa/index'], ['class' => 'btn btn-success']) ?></p>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nompuerto',
            'altura',
            'categoria',
            'pendiente',
            'numetapa',
            'dorsal',
        ],
    ]) ?>

</div>
