<?php
use yii\widgets\DetailView;
use yii\grid\GridView;
use app\models\Puerto;
use app\controllers\PuertoController;
use yii\grid\ActionColumn;
use yii\helpers\Url;
use yii\helpers\Html;

?>
<h2>ETAPA <?=$model->numetapa ?></h2>
<div class="etapa-item" style="display:flex;justify-content: space-evenly;">
    <div class="nombre-imagen column-md-4">
        <?= Html::img("@web/img/bing/etapas.jpg")?>
    </div>
    <div class="column-md-4">
        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            ['attribute' => 'numetapa',
             'label' => 'Etapa'],
            'salida',
            'llegada',
            'kms'
        ],
    ]) ?>
    <p><?= Html::a('Editar Etapa', ['etapa/update', 'numetapa'=> $model->numetapa], ['class'=>'btn btn-outline-primary']) ?></p>
    <p><?= Html::a('Borrar Equipo', ['etapa/delete', 'numetapa'=> $model->numetapa], ['class'=>'btn btn-outline-danger']) ?></p>
    <p><?= Html::a('Añadir Puerto', ['puerto/create'], ['class'=>'btn btn-outline-success']) ?></p>    
    </div>
    <div class="column-md-4">
        <?= GridView::widget([
            'dataProvider' => PuertoController::puertosDeLaEtapa($model->numetapa),
            'columns' => [
                ['attribute' => 'nompuerto',
                'label' => 'Puerto'],
                'altura',
                'categoria',
                'pendiente',
                [
                    'class' => ActionColumn::className(),
                    'urlCreator' => function ($action, $model, $key, $index) {
                        $url = ['puerto/' . $action, 'nompuerto' => $model->nompuerto];
                        return Url::to($url);
                    },
                ],

            ],
            'summary' => '',
        ]); ?>
        
    </div>
</div>

