<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use app\models\Puerto;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\models\Etapa $model */

$this->title = $model->numetapa;
$this->params['breadcrumbs'][] = ['label' => 'Etapas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="etapa-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'numetapa' => $model->numetapa], ['class' => 'btn btn-outline-primary']) ?>
        <?= Html::a('Delete', ['delete', 'numetapa' => $model->numetapa], [
            'class' => 'btn btn-outline-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'numetapa',
            'kms',
            'salida',
            'llegada',
            'dorsal',
        ],
    ]) ?>
    <?= GridView::widget([
        'dataProvider' => $puertos,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nompuerto',
            'altura',
            'categoria',
            'pendiente',
            'numetapa',
            //'dorsal',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Puerto $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'nompuerto' => $model->nompuerto]);
                 }
            ],
        ],
    ]); ?>
    <?= Html::a('Añadir Puerto', ['puerto/create'], ['class' => 'btn btn-outline-success']) ?>

</div>
