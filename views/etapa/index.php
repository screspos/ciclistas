<?php
use yii\helpers\Html;
use yii\widgets\ListView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Etapas';
?>
<div class="etapa-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir Etapa', ['create'], ['class'=>'btn btn-outline-success']) ?>
        <?= Html::a('Volver', ['site/crear'], ['class'=>'btn btn-outline-primary']) ?>
    </p>
   <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_item', 
        'summary' => '', 
        'emptyText' => 'No se encontraron Etapas', 
        'pager' => false,
    ]); ?>
</div>
