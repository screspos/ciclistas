<?php

use app\models\Maillot;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\ListView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Maillots';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maillot-index">

    <h1 class="text-center"><?= Html::encode($this->title) ?></h1>
    
    <?=
    ListView::widget([
        'dataProvider'=>$dataProvider,
        'itemView'=>'_maillot',
        'layout'=>"{items}",
    ]);
    ?>




</div>
