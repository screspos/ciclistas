<?php 
use app\controllers\MaillotController;
use yii\helpers\Html;
use yii\widgets\DetailView;
?>

<h2 class="jumbotron-fluid bg-transparent text-center">Maillot <?= $model->color ?> </h2>
<div class="maillot">
    <div class="nombre-imagen column-md-4">
        <?= Html::img(MaillotController::imagen($model->código))?>
    </div>

    <div class="datos column-md-8">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'código',
                    'tipo',
                    'premio'
                ],
            ]) ?>
        <p class="text-center"><?= MaillotController::descripcion($model->código) ?></p>
    </div>
</div>
