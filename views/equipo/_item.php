<?php
use yii\widgets\DetailView;
use yii\grid\GridView;
use app\controllers\CiclistaController;
use yii\grid\ActionColumn;
use yii\helpers\Url;
use yii\helpers\Html;
use app\controllers\EquipoController;


?>
<div class="etapa-item">
    <h2 class="col-md-6 text-center">EQUIPO <?=$model->nomequipo ?></h2><h2 class="col-md-6 text-center">Ciclistas</h2>
</div>

<div class="etapa-item">
    <div class="nombre-imagen column-md-4">
        <?= Html::img(EquipoController::imagen($model->nomequipo))?>
    </div>
    <div class="column-md-4">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                ['attribute' => 'nomequipo',
                 'label' => 'Nombre'],
                'director',
            ],
        ]) ?>
        <p class="text-center"><?= Html::a('Editar Equipo', ['equipo/update', 'nomequipo'=> $model->nomequipo], ['class'=>'btn btn-outline-primary']) ?></p>
        <p class="text-center"><?= Html::a('Delete', Url::to(['equipo/delete', 'nomequipo' => $model->nomequipo, '_csrf' => Yii::$app->request->csrfToken]), [
            'class'=>'btn btn-outline-danger',
            'data-confirm' => 'Are you sure you want to delete this item?',
            'data-method' => 'post',
                ]); ?></p>
        <p class="text-center"><?= Html::a('Añadir ciclista', ['ciclista/create'], ['class'=>'btn btn-outline-success']) ?></p>    
    </div>
    <div class="column-md-4">
        
        <?= GridView::widget([
            'dataProvider' => CiclistaController::ciclistasDelEquipo($model->nomequipo),
            'columns' => [
                'dorsal',
                'nombre',
                'edad',
                [
                    'class' => ActionColumn::className(),
                    'urlCreator' => function ($action, $model, $key, $index) {
                        $url = ['ciclista/' . $action, 'dorsal' => $model->dorsal];
                        return Url::to($url);
                    },
                ],

            ],
            'summary' => '',
        ]); ?>
        
    </div>
</div>

