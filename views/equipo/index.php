<?php
use yii\helpers\Html;
use yii\widgets\ListView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Equipos';
?>
<div class="equipo-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir Equipo', ['create'], ['class'=>'btn btn-outline-success']) ?>
        <?= Html::a('Volver', ['site/crear'], ['class'=>'btn btn-outline-primary']) ?>
    </p>
   <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_item', 
        'summary' => '', 
        'emptyText' => 'No se encontraron Equipos', 
        'pager' => false,
    ]); ?>
</div>
