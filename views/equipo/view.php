<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use app\models\Ciclista;
use yii\grid\ActionColumn;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\models\Equipo $model */

$this->title = $model->nomequipo;
$this->params['breadcrumbs'][] = ['label' => 'Equipos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="equipo-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'nomequipo' => $model->nomequipo], ['class' => 'btn btn-outline-primary']) ?>
        <?= Html::a('Delete', ['delete', 'nomequipo' => $model->nomequipo], [
            'class' => 'btn btn-outline-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
         <?= Html::a('Volver', ['index'], ['class' => 'btn btn-outline-primary']) ?>
    
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nomequipo',
            'director',
        ],
    ]) ?>
     <?= GridView::widget([
        'dataProvider' => $ciclistas,
        'columns' => [
            [
                'attribute' => 'nombre',
                'header' => 'Corredor',
            ],
            'dorsal',
            'edad',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Ciclista $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'dorsal' => $model->dorsal]);
                },
            ],
        ],
        'rowOptions' => function ($model, $key, $index, $grid) {
            $options = ['class' => ''];

            if ($index < 3) {
                $options['class'] .= ' highlighted-row';
            }

            return $options;
        },
    ]); ?>
    <p>
        <?= Html::a('Añadir Ciclista', ['ciclista/create'], ['class' => 'btn btn-outline-success']) ?>
    </p>
   
</div>
