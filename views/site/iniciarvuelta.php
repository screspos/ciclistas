<?php
use yii\grid\GridView;
use yii\helpers\Html;
/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>
<div style="display: flex;">
    <div class="iniciar">
        <div class="nombre-imagen column-md-4">
            <h2 class="text-center">BIENVENIDOS AL SIMULADOR</h2>
            <?= Html::img('@web/img/bing/ciclistasolimpicos1.jpg')?>
            <p class="text-center">Aqui puedes ver a todos los intrépidos ciclistas que se medirán en una gran
                carrera que les llevará al límite, hasta que finalmente uno de ellos se proclame 
                campeón del Maillot Olímpico. No subestimes a ninguno de los corredores, todos son 
                capaces de ganar esta carrera llena de obstaulos y superación personal.</p>
        </div>

        <div class="nombre-imagen column-md-4">
            <h2 class="text-center">BUENA SUERTE A TODOS</h2>
            <?= Html::img('@web/img/bing/corredores.jpg')?>
            <p class="text-center">Si estás entre los ciclistas, te deseamos la mejor de las suertes, ya que tendrás que 
                competir contra autenticos campeones.<br>
                De todas formas sabemos que eres muy competitivo y que seguro quieres ganar.<br> 
                ¡Da lo mejor de tí para proclamarte campeón!</p>
        </div>
        
        <div class="nombre-imagen column-md-4">
            <h2 class="text-center">SIMULAR VUELTA CICLISTA</h2>
            <?= Html::img('@web/img/bing/correr.jpg')?>
            <p class="text-center">En cuanto presiones el botón INICIAR, comenzará una dura competición
            que enfrentará a todos los ciclistas de los diferentes equipos, compitiendo
            en todas las etapas para conseguir ganar la mayor cantidad de maillots
            posibles y lograr realizar la Vuelta a España en el menor tiempo posible.</p>
            
        </div>
    </div>

    <div style="width: 66.66%;">
        <div class="resultado">
            <h2 class="text-center">MI VUELTA A ESPAÑA</h2>
            <div style="display:flex;justify-content: space-between">
                <div>
                    <h2 class="init text-center">ETAPAS</h2>
                    <?= GridView::widget([
                        'dataProvider' => $etapas,
                        'columns' => [
                            'numetapa',
                            'salida',
                            'llegada',
                            'kms'
                        ],
                        'summary' => '',
                    ]); ?>
                    <p class="text-center"><?=Html::a('INICIAR VUELTA',['site/simular'],['class'=>'btn btn-outline-primary']) ?></p>
                </div>
                <div>
                    <h2 class="text-center">EQUIPOS</h2>
                    <?= GridView::widget([
                        'dataProvider' => $equipos,
                        'columns' => [
                            'nomequipo',
                            'director',
                        ],
                        'summary' => '',
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
