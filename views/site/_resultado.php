<?php
    use app\controllers\SiteController;
    use yii\grid\GridView;
?>

<div style="display: flex; justify-content: space-between">
    <div class="column-md-5">
        <h2 class="text-center">Ciclistas que han ganado el maillot <?= $model->color ?> en cada etapa</h2>
        <?= GridView::widget([
            'dataProvider' => SiteController::ciclistasConMaillots($model->código),
            'columns' => [
                'dorsal',
                'nombre',
                'etapa',
                'salida',
                'llegada'
            ],
            'summary' => '',
        ]); ?>
    </div>
    <div class="column-md-2"> </div>
    <div class="column-md-5">
        <h2 class="text-center">Equipos que han ganado el maillot <?= $model->color ?> en cada etapa</h2>
        <?= GridView::widget([
            'dataProvider' => SiteController::equiposConMaillotsTipo($model->código),
            'columns' => [
                'nomequipo',
                'maillots'
            ],
            'summary' => '',
        ]); ?>
    </div>
    
    
</div>

