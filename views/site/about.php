<?php

/** @var yii\web\View $this */
use yii\helpers\Html;

$this->title = 'El Equipo Noam Chomsky';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="site-about">
    <h1 class="equiponoam text-center"><?= Html::encode($this->title) ?></h1>
        
    <div class="row" >
        <div class="col-md-2">
            
        </div>
        <div class="col-md-8 text-center">
            <p>Bienvenido a la página sobre el Grupo Noam Chomsky. Somos un equipo de desarrolladores comprometidos y apasionados por la creación de soluciones innovadoras.</p>
        </div>
        <div class="col-md-2">
            
        </div>
    </div>

    <div class="members">
        <div class="member">
            <?= Html::img('@web/img/Pablo.jpg', ['alt' => 'Pablo Díaz']) ?>
            <p>Pablo Díaz</p>
        </div>
        <div class="member">
            <?= Html::img('@web/img/Saul.jpg', ['alt' => 'Saúl Crespo']) ?>
            <p>Saúl Crespo</p>
        </div>
        <div class="member">
            <?= Html::img('@web/img/Keyla.jpg', ['alt' => 'Keyla Condori']) ?>
            <p>Keyla Condori</p>
        </div>
        <div class="member">
            <?= Html::img('@web/img/German.jpg', ['alt' => 'Germán Santos']) ?>
            <p>Germán Santos</p>
        </div>
        <div class="member">
            <?= Html::img('@web/img/Yoel.jpg', ['alt' => 'Yoel Suero']) ?>
            <p>Yoel Suero</p>
        </div>
    </div>

    <div class="additional-info">
        <h2 class="equiponoam text-center">Contacto con el Ciclismo</h2>
        
        
        <div class="row" >
            <div class="col-md-2 text-center">

            </div>
            <div class="col-md-8 text-center">
                <p>En Grupo Noam Chomsky, aunque somos principalmente un equipo de desarrollo de software, también nos apasiona el ciclismo. Nos inspira ver a los ciclistas desafiando sus límites y superando obstáculos en cada pedalada. </p>
            </div>
            <div class="col-md-2 text-center">

            </div>
        </div>
    </div>

    <div class="quote-container">
        <div class="quote quote-blue">
            <strong>Pablo Díaz:</strong>
            <p>"El ciclismo no es mi deporte favorito, pero me gusta verlo de vez en cuando"</p>
        </div>
        <div class="quote quote-black">
            <strong>Saúl Crespo:</strong>
            <p>"El ciclismo es una gran forma de desconectar y disfrutar del aire libre"</p>
        </div>
        <div class="quote quote-red">
            <strong>Keyla Condori:</strong>
            <p>"Me encanta la sensación de libertad que experimento al montar en bicicleta"</p>
        </div>
        <div class="quote quote-yellow">
            <strong>Germán Santos:</strong>
            <p>"El ciclismo es un desafío constante, tanto físico como mental"</p>
        </div>
        <div class="quote quote-green">
            <strong>Yoel Suero:</strong>
            <p>"El ciclismo me ayuda a mantenerme en forma y conocer nuevos lugares"</p>
        </div>
    </div>


    <div>
        
        <h2 class="equiponoam text-center opinion">Opinión del proyecto</h2>
        
        
        <div class="row" >
            <div class="col-md-1">
            
            </div>
            <div class="col-md-10 text-center">
                <p>Nos ha gustado trabajar con una base de ciclistas, esto ha dado libertad a nuestra imaginación. 
                Además, usar el framework Yii2 suponía un reto, y ver que conseguíamos añadir un color, una forma o un simple texto, nos alegrábamos mucho.
                <br>Cada parte de este proyecto está realizada con cariño y cuidada al máximo, todos nosotros nos hemos esforzado en hacer una página que destaque por 
                el cuidado en los detalles, tanto en los colores (haciendo referencias a los Juegos Olímpicos), como en los textos, imágenes, tablas...etc.
                <br>Esperamos que os guste nuestro trabajo y que os entre un poco el gusanillo de salir por ahí con la bicicleta a dar una vuelta.</p>
            </div>
            <div class="col-md-1">
            
            </div>
        </div>
    </div>

    
        <h2 class="equiponoam text-center opinion">¡Gracias por visitar nuestra página!</h2>
    
</div>
