<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $resultados array */

$this->title = 'Clasificación';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-clasificacion">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'nombre',
        'equipo',
        'tiempo_total',
        // ... otras columnas según tu modelo de datos
        ['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>
</div>
