<?php

use yii\widgets\ListView;
use yii\grid\GridView;
use app\controllers\SiteController;
use yii\helpers\Html;
?>

<h2 class="align-content-center text-center">Los ciclistas y equipos que han ganado cada maillot al final de la vuelta</h2>
<div class="resultado" style="display: flex; justify-content: space-between">
    <div class="nombre-imagen col-md-4">
        <?= Html::img('@web/img/bing/triunfo.jpg') ?>
    </div>
    <div class="col-md-8">   
        <?=
        GridView::widget([
            'dataProvider' => SiteController::ganadoresMaillotFinal(),
            'columns' => [
                'color',
                'tipo',
                'dorsal',
                'nombre',
                'nomequipo',
                'director'
            ],
            'summary' => '',
        ]);
        ?>
    </div>

</div>
<div  class="resultado">

    <?=
    ListView::widget([
        'dataProvider' => $provider,
        'itemView' => '_resultado',
        'summary' => '',
        'emptyText' => 'No se encontraron Equipos',
        'pager' => false,
    ]);
    ?>
</div>

<div class="resultado" style="display: flex; justify-content: space-between;">
    <div>
        <h2 class="text-center">Cantidad de puertos que ha ganado cada ciclista</h2>
        <?=
        GridView::widget([
            'dataProvider' => SiteController::puertosPorCiclista(),
            'columns' => [
                'dorsal',
                'nombre',
                'Puertos Ganados'
            ],
            'summary' => '',
        ]);
        ?>
    </div>

    <div>
        <h2 class="text-center">Cantidad de puertos que ha ganado cada equipo</h2>
        <?=
        GridView::widget([
            'dataProvider' => SiteController::puertosPorEquipo(),
            'columns' => [
                'nomequipo',
                'Puertos Ganados'
            ],
            'summary' => '',
        ]);
        ?>
    </div>
</div>
<div class="resultado" style="display: flex; justify-content: space-between">
    <div>
        <h2 class="text-center">Cantidad de maillots y dinero que ha ganado cada ciclista</h2>
        <?=
        GridView::widget([
            'dataProvider' => SiteController::maillotsYDineroPorCiclista(),
            'columns' => [
                'dorsal',
                'nombre',
                'maillots',
                'Dinero en premios'
            ],
            'summary' => '',
        ]);
        ?>
    </div>

    <div>
        <h2 class="text-center">Cantidad de maillots y dinero que ha ganado cada equipo</h2>
<?=
GridView::widget([
    'dataProvider' => SiteController::maillotsYDineroPorEquipo(),
    'columns' => [
        'nomequipo',
        'maillots',
        'Dinero ganado'
    ],
    'summary' => '',
]);
?>
    </div>
</div>

