<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
use yii\bootstrap4\Html;
$this->title = 'crear';
?>

<div class="container">
    
    <div class="encabezado">
        <h3>¡Crea tus propios equipos y etapas!</h3>
        <p class="lead">¡Únete a la emoción y la aventura de la Vuelta a España creando tus propios equipos y diseñando etapas emocionantes! Con nuestra herramienta fácil de usar, podrás personalizar cada detalle para adaptarlo a tus preferencias y desafiar tus límites. ¡Deja volar tu imaginación y comienza tu viaje ahora mismo!</p>
    </div>
    
    <div class="row">

        <div class="imagencrear col-sm-6">
            <?= Html::img('@web/img/Logos/equipo-standard.jpg')?>
            <p class="text-center"><?= Html::a('Crear Equipos',['equipo/index'],['class'=>'btn btn-outline-primary'])?></p>
        </div>

        <div class="imagencrear col-sm-6">
            <?= Html::img('@web/img/bing/etapas.jpg')?>
            <p class="text-center"><?= Html::a('Crear Etapas',['etapa/index'],['class'=>'btn btn-outline-primary'])?></p>
        </div>
    </div>
    
    
    <div class="row">
    
    
    
    </div>
</div>



