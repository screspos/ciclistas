<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\web\JsExpression;

$this->title = 'Clasificación';
$this->params['breadcrumbs'][] = $this->title;

$script = <<< JS
    // Selecciona solo los tres primeros registros y aplica estilos al cargar la página
    $(document).ready(function() {
        $('.grid-view tbody tr:lt(1)').css('background-color', '#ffd700'); // Oro para el primer registro
        $('.grid-view tbody tr:gt(0):lt(1)').css('background-color', '#c0c0c0'); // Plata para el segundo registro
        $('.grid-view tbody tr:gt(1):lt(1)').css('background-color', '#cd7f32'); // Bronce para el tercer registro
    });
JS;

$this->registerJs($script);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= Html::encode($this->title) ?></title>
    <link rel="stylesheet" href="site.css">
</head>

<body>
    <div class="container">
        <h1><?= Html::encode($this->title) ?></h1>

        <div class="tabla">
            <?php
            Pjax::begin();
            echo GridView::widget([
                'id' => 'gridview-id',
                'dataProvider' => $dataProvider, // Asegúrate de proporcionar el proveedor de datos adecuado
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'dorsal',
                    'nombre',
                    'nomequipo',
                    'edad',
                    // Agrega más columnas según tu base de datos
                ],
            ]);
            Pjax::end();
            ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="text-muted">&copy; <?= date('Y') ?> Tu Compañía</p>
        </div>
    </footer>
</body>

</html>
