<?php

use app\models\Ciclista;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Clasificación General';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ciclista-index">

    <div class="header" style="background-color: orange; padding: 10px;">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => 'Puesto',
            ],
            [
                'attribute' => 'nombre',
                'header' => 'Corredor',
            ],
            [
                'attribute' => 'nomequipo',
                'header' => 'Equipo',
            ],
            'dorsal',
            'edad',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Ciclista $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'dorsal' => $model->dorsal]);
                },
            ],
        ],
        'rowOptions' => function ($model, $key, $index, $grid) {
            $options = ['class' => ''];

            if ($index < 3) {
                $options['class'] .= ' highlighted-row';
            }

            return $options;
        },
    ]); ?>

    <?php
    $this->registerCss('.highlighted-row { background-color: #FFFFCC; }');
    $this->registerCss('.header { background-color: orange; padding: 10px; }');
    ?>

</div>