<?php

namespace app\controllers;

use app\models\Maillot;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MaillotController implements the CRUD actions for Maillot model.
 */
class MaillotController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Maillot models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Maillot::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'código' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Maillot model.
     * @param string $código Código
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($código)
    {
        return $this->render('view', [
            'model' => $this->findModel($código),
        ]);
    }

    /**
     * Creates a new Maillot model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Maillot();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'código' => $model->código]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Maillot model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $código Código
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($código)
    {
        $model = $this->findModel($código);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'código' => $model->código]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Maillot model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $código Código
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($código)
    {
        $this->findModel($código)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Maillot model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $código Código
     * @return Maillot the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($código)
    {
        if (($model = Maillot::findOne(['código' => $código])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public static function descripcion($codigo) {
        switch($codigo){
            case 'MGE':
                return "Este maillot permite identificarse como el primero de la clasificación general.";
            case 'MMO':
                return "Este maillot es el ofrecido al montañista que lidera la clasificación de la montaña.";
            case 'MMS':
                return"Este maillot se le otorga al ciclista que ha hecho un esfuerzo más notable en una etapa.";
            case 'MMV':
                return "Este maillot que ha pasado primero por más metas colocadas en mitad de la etapa.";
            case 'MRE':
                return"Este maillot se le adjudica al ciclista que lidera la clasificación por puntos.";
            case 'MSE':
                return"Este maillot se le otorga al ciclista que más sprints ha hecho en la etapa.";
            default:
                return "Otra cosa";
        }
    }
    
    public static function imagen($codigo) {
        switch($codigo){
            case 'MGE':
                return "@web/img/bing/maillot-amarillo-ciclista.jpg";
            case 'MMO':
                return "@web/img/bing/maillot-rojiblanco-2.jpg";
            case 'MMS':
                return"@web/img/bing/maillot-estrellas-ciclista.jpg";
            case 'MMV':
                return "@web/img/bing/maillot-rojo.jpg";
            case 'MRE':
                return"@web/img/bing/maillot-verde-pinturas.jpg";
            case 'MSE':
                return"@web/img/bing/maillot-rosa-ciudad.jpg";
            default:
                return "Otra cosa";
        }
    }
}
