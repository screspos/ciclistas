<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Equipo;
use app\models\Ciclista;
use yii\data\ActiveDataProvider;
use app\models\Etapa;
use yii\db\Expression;
use app\controllers\CiclistaController;
use yii\data\SqlDataProvider;
use app\models\Maillot;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionCrear() {
        return $this->render('crear',[
           "Texto" => "Página para crear vueltas"
        ]);
    }
    
    
    public function actionEquipos() {
        $teams = new ActiveDataProvider([
            'query'=>Equipo::find()->select("nomequipo,director"),
            'pagination' => [
                'pageSize' => -1, // Muestro todos los registros.
            ]
        ]);
        
        

        return $this->render('vista', [
            "resultados"=> $teams,
            "campos"=> ['nomequipo','director']
       ]);    
    }
    
    public function actionClasificacion() {
        $dataProvider = new ActiveDataProvider([
           'query' => Ciclista::find()->select("*") 
        ]);
        foreach($dataProvider->getModels() as $model) {
            
        }
        count(Ciclista::hasMany("etapa",['dorsal'])->asArray());
    }
    
    
    
    public function actionSimular()
    {
        $etapas = Etapa::find()->all();
        $equipos = Equipo::find()->all();
        $ciclistas = CiclistaController::inicializar($equipos);
        $ciclistas_arr = $ciclistas->getModels();       
        $datos = [];   
        Yii::$app->db->createCommand('DELETE FROM lleva;')->execute();   
        foreach($etapas as $etapa) { // Por cada etapa
            $index = (string) $etapa->numetapa;
            $minTiempoVolante = $minTiempoPuerto = null;
            $datos[$index] = '<h2 class="etapa-titulo text-center jumbotron-fluid">' . $etapa->salida . ' ' . $etapa->llegada  . ' ' . $etapa->kms . ' Kms</h2>';      
            $puertos = $etapa->setPuertos(); // Colocamos los puertos dentro de la etapa
            $metas_volantes = $etapa->colocarMetasVolantes(); // Colocamos las metas volantes dentro de la etapa
            $etapa->run($etapa,$ciclistas,$datos,$index,$metas_volantes,$minTiempoVolante,$puertos,$minTiempoPuerto); // Simulamos la etapa
            $etapa->resultados($ciclistas, $ciclistas_arr,$datos,$index); // Computamos los resultados 
        }

        return $this->render('simular', [
            'datos' => $datos,
            'clasificacion' => $ciclistas_arr,
            'ciclistas' => $ciclistas,
            'etapas' => $etapas,
            'equipos' => $equipos,
        ]);
    }
 
    public function actionIniciarvuelta()
    {
        $etapas = new ActiveDataProvider([
            'query' => Etapa::find()->select("*"),
            'pagination' => [
                'pageSize' => -1, // Muestro todos los registros.
            ]
        ]);
        $equipos = new ActiveDataProvider([
            'query' => Equipo::find()->select("*"),
            'pagination' => [
                'pageSize' => -1, // Muestro todos los registros.
            ]
        ]);
        return $this->render('iniciarvuelta',[
            'equipos' => $equipos,
            'etapas' => $etapas,
       
        ]);   
    }
    
    public function renderSimEtapa($etapas, $equipos, $ciclistas)
    {
        return $this->render('sim_etapa',[
            'ciclistas' => $ciclistas,
            'etapas' => $etapas,
            'equipos' => $equipos
        ]);
    }
    /**
     * Devuelve en un DataProvider la lista de los ciclistas que han ganado un
     * maillot con un código determinado 
     * 
     * @param type $codigo
     * @return SqlDataProvider
     */
    public static function ciclistasConMaillots($codigo) {
        return new SqlDataProvider([
           'sql' => 'SELECT DISTINCT l.dorsal, c.nombre, e.numetapa AS etapa, e.salida, e.llegada
                        FROM etapa e
                        INNER JOIN lleva l ON e.dorsal=l.dorsal
                        INNER JOIN ciclista c on c.dorsal=l.dorsal
                        INNER JOIN maillot m ON l.código = m.código
                        WHERE m.código ="' . $codigo . '"',
            'pagination' => false
        ]);
    }
    /**Devuelve en un DataProvider la lista de los equipos, con la cantidad de
     * maillots que han ganado de cada tipo
     * 
     * @param type $codigo
     * @return SqlDataProvider
     */
    public static function equiposConMaillotsTipo($codigo) {
        return new SqlDataProvider([
           'sql' => 'SELECT e.nomequipo, COUNT(*) AS maillots 
                        FROM lleva l INNER JOIN ciclista c ON l.dorsal = c.dorsal
                          INNER JOIN equipo e ON c.nomequipo=e.nomequipo
                          INNER JOIN maillot m ON l.código = m.código
                        WHERE m.código="' . $codigo . '"
                        GROUP BY e.nomequipo, l.código
                        ORDER BY m.código asc, maillots DESC',
            'pagination' => false
        ]);
    }
    
    public static function maillotsYDineroPorCiclista() {
        return new SqlDataProvider([
            'sql' => 'SELECT c.dorsal, c.nombre, COUNT(l.dorsal) AS maillots, SUM(m.premio) AS "Dinero en premios"
                        FROM ciclista c left JOIN lleva l ON c.dorsal = l.dorsal
                        LEFT JOIN maillot m ON l.código=m.código
                        GROUP BY c.dorsal
                        ORDER BY 4 desc',
            'pagination' => false
        ]);
    }
    
    public static function maillotsYDineroPorEquipo() {
        return new SqlDataProvider([
            'sql' => 'SELECT  e.nomequipo, COUNT(l.dorsal) AS maillots, SUM(m.premio) AS "Dinero ganado"
                        FROM equipo e INNER JOIN ciclista c USING (nomequipo) LEFT JOIN lleva l ON c.dorsal = l.dorsal
                        LEFT JOIN maillot m ON l.código=m.código
                        GROUP BY e.nomequipo
                        ORDER BY 3 desc',
            'pagination' => false
        ]);
    }
    
    public static function puertosPorCiclista() {
        return new SqlDataProvider([
            'sql' => 'SELECT c.dorsal, c.nombre, COUNT(*) AS "Puertos Ganados"
                        FROM ciclista c INNER JOIN puerto p USING (dorsal)
                        GROUP BY c.dorsal',
            'pagination' => false
        ]);
    }
    
    public static function puertosPorEquipo() {
        return new SqlDataProvider([
            'sql' => 'SELECT c.nomequipo, COUNT(*) AS "Puertos Ganados"
                        FROM ciclista c INNER JOIN puerto p USING (dorsal)
                        GROUP BY c.nomequipo',
            'pagination' => false
        ]);
    }
    
    public static function ganadoresMaillotFinal() {
        return new SqlDataProvider([
            'sql' => 'SELECT m.color, m.tipo, c.dorsal, c.nombre, e.nomequipo, e.director
                        FROM (SELECT * FROM lleva
                        WHERE numetapa=(SELECT MAX(numetapa) FROM etapa))AS l
                        INNER JOIN maillot m USING (código)
                        INNER JOIN ciclista c USING (dorsal)
                        INNER JOIN equipo e USING (nomequipo)
                      UNION
                      SELECT m.color, m.tipo, c.dorsal, c.nombre, e.nomequipo, e.director
                        FROM (SELECT * FROM lleva
                        WHERE numetapa=(SELECT MAX(numetapa) FROM puerto)) AS p
                        INNER JOIN maillot m USING (código)
                        INNER JOIN ciclista c USING (dorsal)
                        INNER JOIN equipo e USING (nomequipo)
                        WHERE m.código="MMO"',
            'pagination' => false
        ]);
    }
    
    
    
    public function actionResultados() {
        $dataProvider = new ActiveDataProvider([
           'query' => Maillot::find()->select("código,color")->distinct() 
        ]);
        return $this->render('resultados',[
            'provider'=> $dataProvider
        ]);
    }
    
}
