<?php

namespace app\controllers;

use app\models\Etapa;
use app\models\Puerto;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;


/**
 * EtapaController implements the CRUD actions for Etapa model.
 */
class EtapaController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Etapa models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find(),
            
            'pagination' => [
                'pageSize' => 50
            ],
            /*
            'sort' => [
                'defaultOrder' => [
                    'numetapa' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Etapa model.
     * @param int $numetapa Numetapa
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($numetapa)
    {
        $puertos = new ActiveDataProvider([
            "query" => Puerto::find()->select("*")->where("numetapa='" . $numetapa . "'")->distinct()
        ]);
        
        return $this->render('view', [
            'model' => $this->findModel($numetapa),
            'puertos' => $puertos
        ]);
    }

    /**
     * Creates a new Etapa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Etapa();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'numetapa' => $model->numetapa]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Etapa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $numetapa Numetapa
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($numetapa)
    {
        $model = $this->findModel($numetapa);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'numetapa' => $model->numetapa]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Etapa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $numetapa Numetapa
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($numetapa)
    {
        $this->findModel($numetapa)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Etapa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $numetapa Numetapa
     * @return Etapa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($numetapa)
    {
        if (($model = Etapa::findOne(['numetapa' => $numetapa])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    /** Realiza la simulacion de una etapa
     * 
     */
    public function actionSimular() {
        $ciclistas = json_decode(Yii::$app->request->post('ciclistas'), true);
        $etapas = json_decode(Yii::$app->request->post('etapas'), true);
        $index = Yii::$app->request->post('index');
        
        $etapa = $etapas->find()->where("ID=" . $index);
        $datos = "<h2>Etapa: " . $etapa->id . " " . $etapa->salida . " " . $etapa->llegada . "</h2>";
        $datos .= '<h2>---------------- ' . $etapa->salida . ' ' . $etapa->llegada  . ' ' . $etapa->kms . ' Kms ----------------------</h2>';
        $puertos = new ActiveDataProvider([
            'query' => Puerto::find()->select('*')->where("numetapa=" . $etapa->numetapa)
        ]);

        $metas_volantes = $etapa->colocarMetasVolantes();

        // Colocamos los puertos en un km aleatorio de la etapa.
        foreach($puertos->getModels() as $puerto) {
            $puerto->km = rand(5,$etapa->kms);
        }
        // Por cada km de la etapa
         for($i = 1; $i<= $etapa->kms; $i++) {
            // Por cada ciclista
            $datos .= "<h2>---- KM " . $i . " ----</h2>";
            foreach($ciclistas->getModels() as $ciclista) {
                // Se suma un tiempo en segundos con un 5% de posibilidades de
                // tener la posibilida de hacer un sprint especial
                $temp = (rand(0,100) > 1)? rand(45,180):rand(36,180);
                // Si el tiempo es muy bajo, hace un sprint
                $ciclista->sprints += ($temp<45)?1:0;
                // Suma los segundos al tiempo de la etapa y al tiempo total.
                $ciclista->tiempo += $temp;
                $ciclista->tiempo_total += $temp;
                // Imprime el tiempo de todos los ciclistas (Debug).
                if ($i%5==0) { // cada 5 Kms (Para no sobrecargar la memoria disponible).
                    $datos .= '<p>' . $ciclista->nombre
                           . ". Tiempo: " . $ciclista->getTiempoEtapa()
                            . " Tiempo Total: " . $ciclista->getTiempoTotal()
                            . (($temp<45)?"¡SPRINT ESPECIAL!":"") . '</p>';
                }
            }
            // Si hay metas volantes en la etapa
            if ($metas_volantes != null) {
                // Por cada meta volante
                foreach($metas_volantes as $volante) {
                    // Si hemos llegado a una meta volante
                    if($i==$volante) {
                        $minTiempoVolante = new Ciclista();
                        $minTiempoVolante->tiempo = 100000000;
                        // Al final de la etapa, por cada ciclista:
                        foreach($ciclistas->getModels() as $ciclista) {
                            // Comprobamos quién ha ganado la etapa (completado en menos tiempo).
                            $minTiempoVolante = ($ciclista->tiempo < $minTiempoVolante->tiempo)? $ciclista:$minTiempoVolante;
                        }
                        // Al ciclista ganador se le suman los puntos del puerto.
                        $minTiempoVolante->metas_volantes += 1;
                    }
                }
            }

            // Comprobamos si han llegado a un puerto
            foreach($puertos->getModels() as $puerto) {
                if ($i==$puerto->km) {
                    // Creamos una Instancia de Ciclista con tiempo máximo para ver quién ha ganado el puerto
                    $minTiempoPuerto = new Ciclista();
                    $minTiempoPuerto->tiempo = 100000000;
                    // Al final de la etapa, por cada ciclista:
                    foreach($ciclistas->getModels() as $ciclista) {
                        // Comprobamos quién ha ganado la etapa (completado en menos tiempo).
                        $minTiempoPuerto = ($ciclista->tiempo < $minTiempoPuerto->tiempo)? $ciclista:$minTiempoPuerto;
                    }
                    // Al ciclista ganador se le suman los puntos del puerto.
                    $minTiempoPuerto->puntos_puerto = $minTiempoPuerto->puntos_puerto + $puerto->getPuntos();
                    // Actualizamos la tabla puerto con el ganador de cada puerto
                    Yii::$app->db->createCommand(
                        'UPDATE puerto SET dorsal=' . $minTiempoPuerto->dorsal . ' WHERE nompuerto="' . $puerto->nompuerto . '";')->execute();
                    $datos .= '<p>El ciclista ' . $minTiempoPuerto->nombre . ' ha ganado el puerto ' . $puerto->nompuerto . 'con un tiempo de ' . $minTiempoPuerto->getTiempoEtapa() . '</p>';
                }
            }


        }

        // Creamos una Instancia de Ciclista con sprints a 0 para ver quién ha hecho más sprints en la etapa
        $maxSprints = new Ciclista();
        $maxSprints->sprints = 0;
        // Creamos una Instancia de Ciclista con tiempo máximo para ver quién ha ganado la etapa
        $minTiempo = new Ciclista();
        $minTiempo->tiempo = 100000000;

        // Creamos una Instancia de Ciclista con puntos_puerto 0 para ver quién ha ganado la montaña.
        $maxPuertos = new Ciclista();
        $maxPuertos->puntos_puerto = 0;

        // Creamos una Instancia de Ciclista con metas_volantes 0 para ver quién ha hecho más metas volantes.
        $maxMetasVolantes= new Ciclista();
        $maxMetasVolantes->metas_volantes = 0;

        // Ordenamos la lista de ciclistas
        $ciclistas_arr = Ciclista::ordenar($ciclistas_arr,"tiempo");

        // asignamos puntos en función del tiempo
        $etapa->assignPoints($ciclistas_arr);

        // mostramos la clasificación de la etapa
        $datos .= '<h2>CLASIFICACIÓN:</h2>';
        $i = 0;
        foreach($ciclistas_arr as $ciclista) {  
            $datos .= '<p>' . ($i + 1) . 'º: ' . $ciclista->nombre . ' ' . $ciclista->getTiempoEtapa() . $ciclista->puntos . '</h2>';
            $i++;
            if ($i>=20) {
                break;
            }
        }


        // Al final de la etapa, por cada ciclista:
        foreach($ciclistas->getModels() as $ciclista) {
            // Comprobamos si ha sido el que más Sprints ha tenido
            $maxSprints = ($ciclista->sprints > $maxSprints->sprints)? clone $ciclista:$maxSprints;
            // Comprobamos quién ha ganado la etapa (completado en menos tiempo).
            $minTiempo = ($ciclista->tiempo < $minTiempo->tiempo)? clone $ciclista:$minTiempo;            
            // Comprobamos si ha sido el que más puntos de puerto ha tenido.
            $maxPuertos = ($ciclista->puntos_puerto > $maxPuertos->puntos_puerto)? clone $ciclista:$maxPuertos;
            // Comprobamos qué ciclista ha obtenido más metas volantes
            $maxMetasVolantes = ($ciclista->metas_volantes > $maxMetasVolantes->metas_volantes)? clone $ciclista:$maxMetasVolantes;
            // Se resetea el tiempo de la etapa
            $ciclista->tiempo = 0;
            $ciclista->puntos_puerto = 0;
            $ciclista->metas_volantes = 0;
            $ciclista->sprints = 0;
        }

        // Asignamos el ciclista más sufrido
        $mas_sufrido = $ciclistas_arr[rand(0,count($ciclistas_arr) - 1)];

        // Guardamos en la tabla lleva el ciclista que ha hecho más sprints especiales de la etapa
        if ($maxSprints->sprints!=0) { // Sólo si alguien ha hecho algún sprint
            Yii::$app->db->createCommand(
            'INSERT INTO lleva(dorsal,numetapa,código) VALUES'
            . '(' . $maxSprints->dorsal . ',' . $etapa->numetapa . ',"MSE")')->execute();
        }
        // Guardamos en la tabla lleva el ciclista que ha ganado la etapa
        Yii::$app->db->createCommand(
            'INSERT INTO lleva(dorsal,numetapa,código) VALUES'
            . '(' . $minTiempo->dorsal . ',' . $etapa->numetapa . ',"MGE")')->execute();
        // Actualizamos la tabla etapa con el ganador de esta etapa.
        Yii::$app->db->createCommand(
            'UPDATE etapa SET dorsal = ' . $minTiempo->dorsal . ' WHERE numetapa = ' . $etapa->numetapa . ';')->execute();

        // Guardamos en la tabla lleva el ciclista que ha ganado el maillot de montaña en esta etapa.
        if ($maxPuertos->puntos_puerto != 0) {
            Yii::$app->db->createCommand(
            'INSERT INTO lleva(dorsal,numetapa,código) VALUES'
            . '(' . $maxPuertos->dorsal . ',' . $etapa->numetapa . ',"MMO")')->execute();
        }

         // Guardamos en la tabla lleva el ciclista que ha ganado el maillot de metas volantes.
        if ($maxMetasVolantes->metas_volantes != 0) {
            Yii::$app->db->createCommand(
            'INSERT INTO lleva(dorsal,numetapa,código) VALUES'
            . '(' . $maxMetasVolantes->dorsal . ',' . $etapa->numetapa . ',"MMV")')->execute();
        }

        // Guardamos en la tabla el ciclista que ha ganado el maillot de regularidad en esta etapa (El que tiene más puntos)
        Yii::$app->db->createCommand(
            'INSERT INTO lleva(dorsal,numetapa,código) VALUES'
            . '(' . $ciclistas_arr[0]->dorsal . ',' . $etapa->numetapa . ',"MRE")')->execute();

        // Guardamos en la tabla el ciclista más sufrido en esta etapa
        Yii::$app->db->createCommand(
            'INSERT INTO lleva(dorsal,numetapa,código) VALUES'
            . '(' . $mas_sufrido->dorsal . ',' . $etapa->numetapa . ',"MMS")')->execute();


//------------------------------------------------------------------------------------------------------------------------
        // Mostramos la clasificación general por tiempo
        $datos .= '<h2>CLASIFICACIÓN GENERAL TIEMPO:</h2>';
        $ciclistas_arr = Ciclista::ordenar($ciclistas_arr,"tiempo_total");
        $i = 0;
        foreach($ciclistas_arr as $ciclista) {  
            $datos .= '<p>' . ($i + 1) . 'º: ' . $ciclista->nombre . ' ' . $ciclista->getTiempoTotal() . '</h2>';
            $i++;                
        }
        $i = 0;
        $datos .= '<h2>CLASIFICACIÓN GENERAL PUNTOS:</h2>';
        $ciclistas_arr = Ciclista::ordenar($ciclistas_arr,"puntos");
        foreach($ciclistas_arr as $ciclista) {  
            $datos .= '<p>' . ($i + 1) . 'º: ' . $ciclista->nombre . ' ' . $ciclista->puntos . '</h2>';
            $i++; 
        }
        
        $this->render('simular',[
            'datos' => $datos,
            'ciclistas' => $ciclistas,
            'etapas' => $etapas,
            'index' => $index + 1
        ]);
    }
}
