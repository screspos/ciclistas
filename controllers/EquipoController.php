<?php

namespace app\controllers;

use app\models\Equipo;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use Yii;
use yii\helpers\Url;
/**
 * EquipoController implements the CRUD actions for Equipo model.
 */
class EquipoController extends Controller
{
    public static $standardimage = '@webroot/path/to/standard-image.jpg';
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Equipo models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Equipo::find(),
            
            'pagination' => [
                'pageSize' => 50
            ],
            /*
            'sort' => [
                'defaultOrder' => [
                    'nomequipo' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Equipo model.
     * @param string $nomequipo Nomequipo
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($nomequipo)
    {
        $ciclistas = new ActiveDataProvider([
            'query' => Ciclista::find()->select("dorsal,nombre,edad")->where("nomequipo='" .$nomequipo . "'")
        ]);
        
        return $this->render('view', [
            'model' => $this->findModel($nomequipo),
            'ciclistas' => $ciclistas
        ]);
    }

    /**
     * Creates a new Equipo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Equipo();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'nomequipo' => $model->nomequipo]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Equipo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $nomequipo Nomequipo
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($nomequipo)
    {
        $model = $this->findModel($nomequipo);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'nomequipo' => $model->nomequipo]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Equipo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $nomequipo Nomequipo
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($nomequipo)
    {
        $this->findModel($nomequipo)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Equipo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $nomequipo Nomequipo
     * @return Equipo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($nomequipo)
    {
        if (($model = Equipo::findOne(['nomequipo' => $nomequipo])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public static function imagen($nomequipo) {
        $imagenes = ['Amore Vita.jpg', 'Artiach.jpg', 'Banesto.jpg',
        'Bresciali-Refin.jpg', 'Carrera.jpg', 'Castorama.jpg',
        'Euskadi.jpg', 'Gatorade.jpg', 'Gewiss.jpg',
        'Lotus Festina.jpg', 'Mercatone Uno.jpg', 'Jolly Club.jpg',
        'Kelme.jpg', 'Motorola.jpg', 'Navigare.jpg',
        'ONCE.jpg', 'PDM.jpg', 'Seguros Amaya.jpg',
        'Telecom.jpg', 'TVM.jpg', 'Wordperfect.jpg'];
        if (in_array($nomequipo . '.jpg', $imagenes)) {
            return Url::to('@web/img/logos/' . $nomequipo . '.jpg');
        } else {
            return Url::to('@web/img/logos/equipo-standard.jpg');
        }
    }
}
