<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ciclista".
 *
 * @property int $dorsal
 * @property string $nombre
 * @property int|null $edad
 * @property string $nomequipo
 *
 * @property Etapa[] $etapas
 * @property Lleva[] $llevas
 * @property Equipo $nomequipo0
 * @property Puerto[] $puertos
 */

class Ciclista extends \yii\db\ActiveRecord
{
    // Tiempo de etapa
    public $tiempo;
    
    // Tiempo de clasificación
    public $tiempo_total;
    
    // Puntos totalas
    public $puntos;
    
    // Sprints por etapa
    public $sprints;
    
    // Sprints por puerto
    public $puntos_puerto;
    
    // Metas volantes
    public $metas_volantes;
    

    /**
     * Devuelve el nombre de la tabla
     */
    public static function tableName() {
        return 'ciclista';
    }

    /**
     * Restricciones de la tabla
     */
    public function rules() {
        return [
            [['dorsal', 'nombre', 'nomequipo'], 'required'], // Campos obligatorios
            [['dorsal', 'edad'], 'integer'], // Campos con numeros enteros
            [['nombre'], 'string', 'max' => 30], // varchar(30)
            [['nomequipo'], 'string', 'max' => 25], // varchar(25)
            [['dorsal'], 'unique'], // Dorsal es la Primay Key
            [['nomequipo'], 'exist', 'skipOnError' => true, 'targetClass' =>
                Equipo::class, 'targetAttribute' => ['nomequipo' => 'nomequipo']],
                // Clave foránea.
        ];
    }

    /**
     * Devuelve un array asociativo con los nombres de los atributos
     */
    public function attributeLabels() {
        return [
            'dorsal' => 'Dorsal',
            'nombre' => 'Nombre',
            'edad' => 'Edad',
            'nomequipo' => 'Nomequipo',
        ];
    }

    // Funciones para unir esta clase con las clases de las tablas relacionadas.

    /**
     * Gets query for [[Etapas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEtapas() {
        return $this->hasMany(Etapa::class, ['dorsal' => 'dorsal']);
    }
    
    public function getNombre() {
        return $this->hasMany(Nombre::class, ['dorsal' => 'dorsal']);
    }

    /**
     * Gets query for [[Llevas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLlevas() {
        return $this->hasMany(Lleva::class, ['dorsal' => 'dorsal']);
    }

    /**
     * Gets query for [[Nomequipo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNomequipo0() {
        return $this->hasOne(Equipo::class, ['nomequipo' => 'nomequipo']);
    }

    /**
     * Gets query for [[Puertos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPuertos() {
        return $this->hasMany(Puerto::class, ['dorsal' => 'dorsal']);
    }

    
    
    /**
     * Devuelve el tiempo en Horas, minutos y segundos
     * @return type
     */
    private function getTiempo($tiempo) {
        $hours = floor($tiempo/3600);
        $rest = $this->tiempo%3600;
        $minutes = floor($rest/60);
        return $hours . ' h ' . $minutes . ' m ' . $rest%60 . ' s.';
    }
    
    public function getTiempoEtapa() {
        return $this->getTiempo($this->tiempo);
    }
    
    public function getTiempoTotal() {
        return $this->getTiempo($this->tiempo_total);
    }
  
    public static function compareTiempo($a, $b) {
        return $a->tiempo - $b->tiempo;
    }
    
    public static function compareTiempoTotal($a, $b) {
        return $a->tiempo_total - $b->tiempo_total;
    }
    
    public static function comparePuntos($a, $b) {
        return $b->puntos - $a->puntos;
    }

    public static function ordenar($ciclistas,$campo) {
        switch($campo) {
            case "tiempo":
                usort($ciclistas, array('app\models\Ciclista', 'compareTiempo'));
                break;
            case "tiempo_total":
                usort($ciclistas, array('app\models\Ciclista', 'compareTiempoTotal'));
                break;
            case "puntos":
                usort($ciclistas, array('app\models\Ciclista', 'comparePuntos'));
                break;
        }   
        return $ciclistas;
    }
}