<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
class Vuelta {
    public $first;
    /**
     * Método para enlazar las diferentes etapas de una vuelta.
     * 
     * @param type $primera La primera Etapa
     * @param type $resto Array con el resto de Etapas
     */
    public function link($primera, $resto){
        $first = $primera;
        $actual = $first;
        
        for ($i = 1; $i < count($resto); $i++) {
            $actual->setNext($resto[$i]);
            $actual = $resto[$i];
        }
    }
}
