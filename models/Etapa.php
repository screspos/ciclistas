<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "etapa".
 *
 * @property int $numetapa
 * @property int $kms
 * @property string $salida
 * @property string $llegada
 * @property int|null $dorsal
 *
 * @property Maillot[] $códigos
 * @property Ciclista $dorsal0
 * @property Lleva[] $llevas
 * @property Puerto[] $puertos
 */
class Etapa extends \yii\db\ActiveRecord
{
    
    public $status_1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'etapa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numetapa', 'kms', 'salida', 'llegada'], 'required'],
            [['numetapa', 'kms', 'dorsal'], 'integer'],
            [['salida', 'llegada'], 'string', 'max' => 35],
            [['numetapa'], 'unique'],
            [['dorsal'], 'exist', 'skipOnError' => true, 'targetClass' => Ciclista::class, 'targetAttribute' => ['dorsal' => 'dorsal']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'numetapa' => 'Numetapa',
            'kms' => 'Kms',
            'salida' => 'Salida',
            'llegada' => 'Llegada',
            'dorsal' => 'Dorsal',
        ];
    }

    /**
     * Gets query for [[Códigos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCódigos()
    {
        return $this->hasMany(Maillot::class, ['código' => 'código'])->viaTable('lleva', ['numetapa' => 'numetapa']);
    }

    /**
     * Gets query for [[Dorsal0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDorsal0()
    {
        return $this->hasOne(Ciclista::class, ['dorsal' => 'dorsal']);
    }

    /**
     * Gets query for [[Llevas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLlevas()
    {
        return $this->hasMany(Lleva::class, ['numetapa' => 'numetapa']);
    }

    /**
     * Gets query for [[Puertos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPuertos()
    {
        return $this->hasMany(Puerto::class, ['numetapa' => 'numetapa']);
    }
    
    public function assignPoints($ciclistas) {
        $puntos = [100,75,60,50,40,35,30,25,20,15,12,9,8,7,6,5,4,3,2,1];
        $i = 0;
        foreach($ciclistas as $ciclista) {
            $ciclista->puntos += $puntos[$i];
            $i++;
            if ($i>=count($puntos)) {
                break;
            }
        }
    }
    
    public function colocarMetasVolantes() {
        if ($this->kms < 30) {
            return null;
        }
        
        $metas = Array();
        $num = random_int(0, 10);
        for($i = 0; $i < $num; $i++) {
            $metas[$i] = random_int(10, $this->kms - 10);
        }      
        return $metas;
    }
    
    public function run($etapa,$ciclistas,&$datos,&$index,$metas_volantes,$minTiempoVolante,$puertos,$minTiempoPuerto) {
        $datos[$index] .= '<div class="etapa-log">';
        for($i = 1; $i<= $etapa->kms; $i++) {
                // Asignamos un tiempo recorrido a los ciclistas
                $this->assignTime($ciclistas, $datos[$index], $i);
                // Si hay metas volantes en la etapa
                $this->checkMetasVolantes($metas_volantes,$i,$ciclistas,$minTiempoVolante);
                // Comprobamos si han llegado a un puerto
                $this->checkPuertos($puertos,$i,$minTiempoPuerto,$ciclistas,$datos[$index]);
        }
        $datos[$index] .= '</div>';
    }
    
    public function resultados($ciclistas, &$ciclistas_arr,&$datos,$index) {
        $datos[$index] .= '<div class="etapa-resultados text-center jumbotron-fluid">';
        $this->mostrarClasificacion($ciclistas_arr,$datos[$index]);
        $this->computarResultados($ciclistas, $ciclistas_arr);
        $this->mostrarClasificacionGeneral($datos[$index],$ciclistas_arr);
        $datos[$index] .= '</div>';
    }
    
    public function assignTime($ciclistas, &$datosindex,$i) {
        $datosindex .= ($i%5==0)?'<h2 class="text-center jumbotron-fluid">KM ' . $i . '</h2>' :'';
        foreach($ciclistas->getModels() as $ciclista) {
                    // Se suma un tiempo en segundos con un 5% de posibilidades de
                    // tener la posibilida de hacer un sprint especial
                    $temp = (rand(0,100) > 1)? rand(45,180):rand(36,180);
                    // Si el tiempo es muy bajo, hace un sprint
                    $ciclista->sprints += ($temp<45)?1:0;
                    // Suma los segundos al tiempo de la etapa y al tiempo total.
                    $ciclista->tiempo += $temp;
                    $ciclista->tiempo_total += $temp;
                    // Imprime el tiempo de todos los ciclistas (Debug).
                    if ($i%5==0) { // cada 5 Kms (Para no sobrecargar la memoria disponible).
                        $datosindex .= '<p class="text-center jumbotron-fluid">' .$ciclista->nombre
                               . ". Tiempo: " . $ciclista->getTiempoEtapa()
                                . " Tiempo Total: " . $ciclista->getTiempoTotal()
                                . (($temp<45)?"¡SPRINT ESPECIAL!":"") . '</p>';
                    }
                }
    }
    
    public function checkMetasVolantes($metas_volantes,$i,$ciclistas,$minTiempoVolante) {
        if ($metas_volantes == null) {return false;}
        foreach($metas_volantes as $volante) {
            if($i!=$volante) {continue;}
            $minTiempoVolante = new Ciclista();
            $minTiempoVolante->tiempo = 100000000;
            // Al final de la etapa, por cada ciclista:
            foreach($ciclistas->getModels() as $ciclista) {
                // Comprobamos quién ha ganado la etapa (completado en menos tiempo).
                $minTiempoVolante = ($ciclista->tiempo < $minTiempoVolante->tiempo)? $ciclista:$minTiempoVolante;
            }
            // Al ciclista ganador se le suman los puntos del puerto.
            $minTiempoVolante->metas_volantes += 1;       
        }
    }

    
    public function checkPuertos($puertos,$i,$minTiempoPuerto,$ciclistas,&$datosindex) {
        // Comprobamos si han llegado a un puerto
        foreach($puertos->getModels() as $puerto) {
            if ($i!=$puerto->km) {
                continue;
            }
            $minTiempoPuerto = new Ciclista();
            $minTiempoPuerto->tiempo = 100000000;
            // Al final de la etapa, por cada ciclista:
            foreach($ciclistas->getModels() as $ciclista) {
                // Comprobamos quién ha ganado la etapa (completado en menos tiempo).
                $minTiempoPuerto = ($ciclista->tiempo < $minTiempoPuerto->tiempo)? $ciclista:$minTiempoPuerto;
            }
            // Al ciclista ganador se le suman los puntos del puerto.
            $minTiempoPuerto->puntos_puerto = $minTiempoPuerto->puntos_puerto + $puerto->getPuntos();
            // Actualizamos la tabla puerto con el ganador de cada puerto
            Yii::$app->db->createCommand(
                'UPDATE puerto SET dorsal=' . $minTiempoPuerto->dorsal . ' WHERE nompuerto="' . $puerto->nompuerto . '";')->execute();
            $datosindex .= '<p>El ciclista ' . $minTiempoPuerto->nombre . ' ha ganado el puerto ' . $puerto->nompuerto . 'con un tiempo de ' . $minTiempoPuerto->getTiempoEtapa() . '</p>';
            
        }
    }
    
    public function setPuertos() {
        $puertos = new ActiveDataProvider([
                'query' => Puerto::find()->select('*')->where("numetapa=" . $this->numetapa)
            ]);
        // Colocamos los puertos en un km aleatorio de la etapa.
        foreach($puertos->getModels() as $puerto) {
            $puerto->km = rand(5,$this->kms);
        }
        return $puertos;
    }
    
    public function mostrarClasificacion(&$ciclistas_arr,&$datosindex) {
        // Ordenamos la lista de ciclistas
            $ciclistas_arr = Ciclista::ordenar($ciclistas_arr,"tiempo");
            
            // asignamos puntos en función del tiempo
            $this->assignPoints($ciclistas_arr);
            
            // mostramos la clasificación de la etapa
            $datosindex .= '<h2 class="titulo-resultados">RESULTADOS</h2><div class="etapa-resultado"><h2>RESULTADO ETAPA:</h2>';
            $i = 0;
            foreach($ciclistas_arr as $ciclista) {  
                $datosindex .= '<p>' . ($i + 1) . 'º: ' . $ciclista->nombre . ' ' . $ciclista->getTiempoEtapa() . $ciclista->puntos . '</h2>';
                $i++;
                if ($i>=20) {
                    break;
                }
            }
    }
    
    public function computarResultados($ciclistas, $ciclistas_arr) {
        // Creamos Instancias de ciclistas para saber quién gana cada maillot
        $maxSprints = $minTiempo = $maxPuertos = $maxMetasVolantes = new Ciclista();
        $maxSprints->sprints = 0;
        $minTiempo->tiempo = 100000000;
        $maxPuertos->puntos_puerto = 0;
        $maxMetasVolantes->metas_volantes = 0;

        // Al final de la etapa, por cada ciclista:
        foreach($ciclistas->getModels() as $ciclista) {
            // Comprobamos si ha sido el que más Sprints ha tenido
            $maxSprints = ($ciclista->sprints > $maxSprints->sprints)? clone $ciclista:$maxSprints;
            // Comprobamos quién ha ganado la etapa (completado en menos tiempo).
            $minTiempo = ($ciclista->tiempo < $minTiempo->tiempo)? clone $ciclista:$minTiempo;            
            // Comprobamos si ha sido el que más puntos de puerto ha tenido.
            $maxPuertos = ($ciclista->puntos_puerto > $maxPuertos->puntos_puerto)? clone $ciclista:$maxPuertos;
            // Comprobamos qué ciclista ha obtenido más metas volantes
            $maxMetasVolantes = ($ciclista->metas_volantes > $maxMetasVolantes->metas_volantes)? clone $ciclista:$maxMetasVolantes;
            // Se resetea el tiempo de la etapa
            $ciclista->tiempo = 0;
            $ciclista->puntos_puerto = 0;
            $ciclista->metas_volantes = 0;
            $ciclista->sprints = 0;
        }

        // Asignamos el ciclista más sufrido
        $mas_sufrido = $ciclistas_arr[rand(0,count($ciclistas_arr) - 1)];

        // Guardamos en la tabla lleva el ciclista que ha hecho más sprints especiales de la etapa
        if ($maxSprints->sprints!=0) { // Sólo si alguien ha hecho algún sprint
            Yii::$app->db->createCommand(
            'INSERT INTO lleva(dorsal,numetapa,código) VALUES'
            . '(' . $maxSprints->dorsal . ',' . $this->numetapa . ',"MSE")')->execute();
        }
        // Guardamos en la tabla lleva el ciclista que ha ganado la etapa
        Yii::$app->db->createCommand(
            'INSERT INTO lleva(dorsal,numetapa,código) VALUES'
            . '(' . $minTiempo->dorsal . ',' . $this->numetapa . ',"MGE")')->execute();
        // Actualizamos la tabla etapa con el ganador de esta etapa.
        Yii::$app->db->createCommand(
            'UPDATE etapa SET dorsal = ' . $minTiempo->dorsal . ' WHERE numetapa = ' . $this->numetapa . ';')->execute();

        // Guardamos en la tabla lleva el ciclista que ha ganado el maillot de montaña en esta etapa.
        if ($maxPuertos->puntos_puerto != 0) {
            Yii::$app->db->createCommand(
            'INSERT INTO lleva(dorsal,numetapa,código) VALUES'
            . '(' . $maxPuertos->dorsal . ',' . $this->numetapa . ',"MMO")')->execute();
        }

         // Guardamos en la tabla lleva el ciclista que ha ganado el maillot de metas volantes.
        if ($maxMetasVolantes->metas_volantes != 0) {
            Yii::$app->db->createCommand(
            'INSERT INTO lleva(dorsal,numetapa,código) VALUES'
            . '(' . $maxMetasVolantes->dorsal . ',' . $this->numetapa . ',"MMV")')->execute();
        }

        // Guardamos en la tabla el ciclista que ha ganado el maillot de regularidad en esta etapa (El que tiene más puntos)
        Yii::$app->db->createCommand(
            'INSERT INTO lleva(dorsal,numetapa,código) VALUES'
            . '(' . $ciclistas_arr[0]->dorsal . ',' . $this->numetapa . ',"MRE")')->execute();

        // Guardamos en la tabla el ciclista más sufrido en esta etapa
        Yii::$app->db->createCommand(
            'INSERT INTO lleva(dorsal,numetapa,código) VALUES'
            . '(' . $mas_sufrido->dorsal . ',' . $this->numetapa . ',"MMS")')->execute();
    }
    
    public function mostrarClasificacionGeneral(&$datosindex,$ciclistas_arr) {
        // Mostramos la clasificación general por tiempo
        $datosindex .= '<h2>CLASIFICACIÓN GENERAL TIEMPO:</h2>';
        $ciclistas_arr = Ciclista::ordenar($ciclistas_arr,"tiempo_total");
        $i = 0;
        foreach($ciclistas_arr as $ciclista) {  
            $datosindex .= '<p>' . ($i + 1) . 'º: ' . $ciclista->nombre . ' ' . $ciclista->getTiempoTotal() . '</h2>';
            $i++;                
        }
        $i = 0;
        $datosindex .= '<h2>CLASIFICACIÓN GENERAL PUNTOS:</h2>';
        $ciclistas_arr = Ciclista::ordenar($ciclistas_arr,"puntos");
        foreach($ciclistas_arr as $ciclista) {  
            $datosindex .= '<p>' . ($i + 1) . 'º: ' . $ciclista->nombre . ' ' . $ciclista->puntos . '</h2>';
            $i++; 
        }
        $datosindex .= "</div>";
    }
}